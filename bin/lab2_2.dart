import 'package:lab2_2/lab2_2.dart' as lab2_2;
import 'dart:io';
import 'dart:math';

void main() {
  var prizeList = {
    '1stPrize': '837295',
    '2stPrize': '471926',
    '3stPrize': '274988',
    '4stPrize': '638237',
    '5stPrize': '591725',
    '6stPrize': '284946',
    '7stPrize': '194727',
    '8stPrize': '295722',
    '9stPrize': '582745',
    '10stPrize': '194728'
  };
  print('Enter number');
  var n = stdin.readLineSync()!;
  if(isWin(n, prizeList)) {
    print('$n is win in ${getPrize(n, prizeList)}');
  } else {
    print('$n is not win');
  }
}

bool isWin(n, prizeList) {
  for(var value in prizeList.values) {
    if(n == value) {
      return true;
    }
  }
  return false;
}

String getPrize(n, prizeList) {
  var prize = prizeList.keys.firstWhere((k) => prizeList[k] == '$n');
  return prize;
}